package com.technonebula.math

import com.technonebula.math.trigonometry.Triangle
import com.technonebula.math.trigonometry.TriangleProcessor
import org.junit.Test

class Sandbox {

  @Test
  fun test() {
    val angle = true

    var triangle = Triangle(
        angleA = null, lengthA = 15,
        angleB = null, lengthB = 60,
        angleC = null, lengthC = 50)

    if (!angle) {
      triangle = TriangleProcessor.solveForLengthA(triangle)

      println(triangle.getLength("a"))
    } else {
      triangle = TriangleProcessor.solveForAngleA(triangle)

      println(triangle.getAngle("a"))
    }
  }
}