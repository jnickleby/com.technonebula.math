package com.technonebula.math.trigonometry

import java.lang.Math.toRadians
import java.lang.Math.toDegrees
import java.lang.Math.pow
import kotlin.math.*

/**
 *  Takes in a known set of Angle and Length and the know Angle for which
 *  we want to find the opposite Length of and solves for that length using
 *  the Law of Sines in Degrees.
 *
 *
 *
 *  sin θ   sin φ       x        ?                x       ?
 *  ————— = ————— --> ————— = ————— --> sin φ • ————— = ————— • sin φ
 *    x       ?       sin θ    sin φ            sin θ   sin φ
 *
 *            x
 *  sin φ • ————— = ?
 *          sin θ
 *
 *  see footnotes for more information 1
 */
fun solveLawOfSinesLengthWithDegrees(knownAngleLength: AngleLength, targetAngle: Double): Double {
  val xOverSinTheta = knownAngleLength.xOverSinTheta()
  val sinTargetAngle = sin(toRadians(targetAngle))

  return sinTargetAngle * xOverSinTheta
}

/**
 *  Takes in a known set of Angle and Length and the know Length for which
 *  we want to find the opposite Angle of and solves for that angle using
 *  the Law of Sines in Degrees.
 *
 *
 *
 *  sin θ   sin ?     sin θ
 *  ————— = ————— -->
 *    a       b
 *
 *         ⎰    sin θ ⎱
 *  arccos   b • —————   = ?
 *         ⎱      a   ⎰
 *
 *  see footnotes for more information
 */
fun solveLawOfSinesAngleAsDegrees(knownAngleLength: AngleLength, targetLength: Double): Double {
  val sinThetaOverX = knownAngleLength.sinThetaOverX()

  return toDegrees(asin(targetLength * sinThetaOverX))
}

/**
 *   2    2    2
 *  c  = a  + b  - 2ab cos θ
 *
 *  see footnotes for more information
 */
fun solveLawOfCosinesLengthWithDegrees(targetAngle: Double, lengthA: Double, lengthB: Double): Double {
  val a2 = pow(lengthA, 2.0)
  val b2 = pow(lengthB, 2.0)
  val twoAB = 2 * lengthA * lengthB
  val cos = cos(toRadians(targetAngle))

  return sqrt(a2 + b2 - twoAB * cos)
}

/**
 *   2   2    2                     2    2    2    2               2          2   2   2   2               2
 *  c = a + b  - 2ab cos ?   -->   c  - a  = a  + b - 2ab cos ? - a    -->   c - a - b = b - 2ab cos ? - b
 *
 *   2   2   2                         2   2   2                       ⎰  2   2   2 ⎱
 *  c - a - b  =  - 2ab cos ?   -->   c - a - b  = cos ?   -->   arccos   c - a - b   = ?
 *  ——————————    ———————————         ——————————                         ———————————
 *    - 2ab         - 2ab               - 2ab                          ⎱  - 2ab     ⎰
 *
 *  see footnotes for more information
 */
fun solveLawOfCosinesAngleAsDegrees(targetLength: Double, lengthA: Double, lengthB: Double): Double {
  val a2 = pow(lengthA, 2.0)
  val b2 = pow(lengthB, 2.0)
  val c2 = pow(targetLength, 2.0)
  val top = c2 - a2 - b2
  val bottom = -2 * lengthA * lengthB

  val quotient = top / bottom

  val rads = acos(quotient)

  return toDegrees(rads)
}

/*****************************************/
/**              FOOTNOTES              **/
/*****************************************/

/**
 *  General notes:
 *
 *    1. solveLawOfSinesLengthWithDegrees
 *      a.  solving for x
 *
 *         ^
 *        37°
 *       /  \   5
 *     56°___θ
 *        x
 *
 *        ^                        ^
 *       37°                      37°
 *   x  /  \   5      ->      x  /  \   5
 *    56°___θ                  56°___87°
 *
 */
fun empty() {}