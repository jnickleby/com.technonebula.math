package com.technonebula.math.trigonometry


/**
 *  Useful
 */
class TriangleProcessor {

  companion object {
    fun solveForLength(triangle: Triangle, side: String): Triangle {
      val x = if (side == "a") triangle.a.x else if (side == "b") triangle.b.x else triangle.c.x

      if (x == null) {
        val length: Double? = if (triangle.sufficientLawOfCosine()) {
          println("Solving with Law of Cosines")
          val openLengths = if (side == "a") Pair("b", "c") else if (side == "b") Pair("a", "c") else Pair("a", "b")

          solveLawOfCosinesLengthWithDegrees(triangle.getAngle(side)!!,
                                             triangle.getLength(openLengths.first)!!,
                                             triangle.getLength(openLengths.second)!!)
        } else if (triangle.sufficientLawOfSine()) {
          println("Solving with Law of Sine")
          val knownAngleLength = triangle.getKnownAngleLength()
          val targetAngle = triangle.getAngle(side)

          solveLawOfSinesLengthWithDegrees(knownAngleLength, targetAngle!!)
        } else {
          null
        }

        triangle.setLength(side, length)
      }

      return triangle
    }


    fun solveForAngleA(triangle: Triangle): Triangle {
      return if(triangle.a.theta != null) {
        triangle
      } else if(triangle.sufficientLawOfSine()) {
        val knowAngleLength = triangle.getKnownAngleLength()
        val aAngle = solveLawOfSinesAngleAsDegrees(knowAngleLength, triangle.getLength("a")!!)

        triangle.setAngle("a", aAngle)
      } else if(triangle.sufficientLawOfCosine()) {
        val aAngle = solveLawOfCosinesAngleAsDegrees(
            triangle.getLength("a")!!, triangle.getLength("b")!!, triangle.getLength("c")!!)

        triangle.setAngle("a", aAngle)
      } else {
        triangle
      }
    }
  }
}