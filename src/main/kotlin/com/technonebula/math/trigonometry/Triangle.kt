package com.technonebula.math.trigonometry

enum class TriangleType {
  RIGHT, ACUTE, OBTUSE
}

class Triangle(
    private var lengthA: Double? = null, private var angleA: Double? = null,
    private var lengthB: Double? = null, private var angleB: Double? = null,
    private var lengthC: Double? = null, private var angleC: Double? = null,
    val type: TriangleType = TriangleType.RIGHT) {

  // used to contain the pairs of angle/opposite side's length
  val a: AngleLength = AngleLength(angleA, lengthA)
  val b: AngleLength = AngleLength(angleB, lengthB)
  val c: AngleLength = AngleLength(angleC, lengthC)

  // natural state requires Double so this will take an int and sanitize it for the object
  constructor(
      lengthA: Int? = null, angleA: Int? = null,
      lengthB: Int? = null, angleB: Int? = null,
      lengthC: Int? = null, angleC: Int? = null) :
      this(
          lengthA?.toDouble(), angleA?.toDouble(),
          lengthB?.toDouble(), angleB?.toDouble(),
          lengthC?.toDouble(), angleC?.toDouble()) {
    a.x = lengthA?.toDouble()
    a.theta = angleA?.toDouble()
    b.x = lengthB?.toDouble()
    b.theta = angleB?.toDouble()
    c.x = lengthC?.toDouble()
    c.theta = angleC?.toDouble()
  }

  fun hasAllLengths(): Boolean = lengthA != null && lengthB != null && lengthC != null

  fun listOfAngleLengths(): List<AngleLength> = listOf(a,b,c)
  fun listOfLengths(): List<Double?> = listOf(lengthA, lengthB, lengthC)
  fun listOfAngles(): List<Double?> = listOf(angleA, angleB, angleC)

  /**
   * Counts number of angles and lines and returns them in a Pair
   */
  fun countAnglesAndLengths(): Pair<Int, Int> = listOfAngleLengths().map {
    val hasAngle = if (it.theta != null) 1 else 0
    val hasLength = if (it.x != null) 1 else 0

    Pair(hasAngle, hasLength)
  }.reduce { acc, pair -> Pair(acc.first + pair.first, acc.second + pair.second) }


  /**
   *  To prove sufficient to solve with the Law of Sines it must have either:
   *
   *    more than one angle and more than zero line lengths or
   *    more than one line length and more than zero angles
   *
   *  @see TriangleLaws.solveLawOfSinesLengthWithDegrees and
   *  @see TriangleLaws.solveLawOfSinesAngleAsDegrees
   */
  fun sufficientLawOfSine(): Boolean {
    val totalAngleLength = countAnglesAndLengths()

    return (totalAngleLength.first > 1 && totalAngleLength.second > 0) ||
           (totalAngleLength.first > 0 && totalAngleLength.second > 1)
  }

  /**
   *  To prove sufficient to solve with the Law of Cosines it must have either:
   *
   *    three known line lengths or two line lengths and the opposite angle of the target
   *
   *  @see TriangleLaws.solveLawOfCosinesLengthWithDegrees and
   *  @see TriangleLaws.solveLawOfCosinesAngleAsDegrees
   */
  fun sufficientLawOfCosine(): Boolean {
    val totalAngleLength = countAnglesAndLengths()

    return totalAngleLength.first == 0 || (totalAngleLength.first > 0 && totalAngleLength.second > 1)
  }

  fun getKnownAngleLength(): AngleLength = listOfAngleLengths().filter { it.complete() }.first()

  fun getLength(side: String): Double? = when(side) {
    "a" -> lengthA
    "b" -> lengthB
    "c" -> lengthC
    else -> null
  }

  fun getAngle(side: String): Double? = when(side) {
    "a" -> angleA
    "b" -> angleB
    "c" -> angleC
    else -> null
  }

  fun setLength(side: String, amount: Double?): Triangle {
    when(side) {
      "a" -> {
        lengthA = amount
        a.x = amount
      }
      "b" -> {
        lengthB = amount
        b.x = amount
      }
      "c" -> {
        lengthC = amount
        c.x = amount
      }
    }

    return this
  }

  fun setAngle(side: String, amount: Double?): Triangle {
    when(side) {
      "a" -> {
        angleA = amount
        a.theta = amount
      }
      "b" -> {
        angleB = amount
        a.theta = amount
      }
      "c" -> {
        angleC = amount
        a.theta = amount
      }
    }

    return this
  }
}