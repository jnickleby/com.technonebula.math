package com.technonebula.math.trigonometry

import java.lang.Math.toRadians
import kotlin.math.sin

/**
 * Contains an angle of a triangle along with its opposite side length
 */
class AngleLength(var theta: Double? = null, var x: Double? = null, degrees: Boolean = true) {

  /**
   *    x                sin θ
   *  ————— = inverse of —————
   *  sin θ                x
   */
  fun xOverSinTheta(): Double = if (complete()) x!! / sin(toRadians(theta!!)) else 0.0

  /**
   *  sin θ                x
   *  ————— = inverse of —————
   *    x                sin θ
   */
  fun sinThetaOverX(): Double = if (complete()) sin(toRadians(theta!!)) / x!! else 0.0

  /** has angle and length */
  fun complete(): Boolean = theta != null && x != null
  fun hasOnlyAngle(): Boolean = theta != null && x == null
  fun hasOnlyLength(): Boolean = x != null && theta == null
}
